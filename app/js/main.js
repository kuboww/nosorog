$(document).ready(function(){

//hamburger
    $('#hamburger').click(function(){
        $(this).toggleClass('open');

    });

    $('main').click(function(){
        $('#hamburger').removeClass('open');
        $('#nav').removeClass('in');
    });

    $('.slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span>&rang;</span></div>',
        prevArrow: '<div class="slick-arrow-left"><span>&lang;</span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        // autoplay:true,

        responsive: [
            // {
            //   breakpoint: 1024,
            //   settings: {
            //     slidesToShow: 3,
            //     slidesToScroll: 3,
            //     infinite: true,
            //     dots: true
            //   }
            // },
            // {
            //   breakpoint: 600,
            //   settings: {
            //     slidesToShow: 2,
            //     slidesToScroll: 2
            //   }
            // },
            // {
            //   breakpoint: 480,
            //   settings: {
            //     slidesToShow: 1,
            //     slidesToScroll: 1
            //   }
            // }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });
    });
